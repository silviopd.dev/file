'use strict';

const express = require('express')
const { Client  } = require('pg')
const multer = require('multer') //<-----

const connectionData = {
    user: 'postgres',
    host: 'localhost',
    database: 'z',
    password: 'admin',
    port: 5432,
  }
const client = new Client(connectionData)

var storage = multer.memoryStorage() //<-----
var upload = multer({ storage: storage }).single("file") //<-----

const app = express();

/**************/
app.post('/upload',upload, (req, res) => {
    console.log(req)

    client.connect()
    client.query('insert into datos(photo,nombre) values($1,$2)',[req.file.buffer,req.body.name])
        .then(response => {
            res.status(200).send('La API funciona correctamente');
            client.end()
        })
        .catch(err => {
            res.status(500).send(err);
            client.end()
        })
});

app.get('/read', (req, res) => {
    console.log(req)

    client.connect()
    client.query(`select encode(photo, 'base64') from datos where id=5;`) //<--- convierte bytea a base 64
        .then(response => {
            console.log(response.rows[0].encode)
            res.status(200).send(`<div>
            <p>Taken from wikpedia</p>
            <img src="data:image/png;base64,${response.rows[0].encode}" alt="Red dot" />
          </div>`);
            client.end()
        })
        .catch(err => {
            res.status(500).send(err);
            client.end()
        })
});
/**************/

app.use('/', (req, res) => {
    res.status(200).send('La API funciona correctamente');
  });

app.listen(3000);